import pandas as pd
from datetime import datetime
import csv
from openpyxl import *
'''
Asking for filename - rename to simpler if easier - give path if not in same folder
'''
filename = input("What is the filename?")
'''
For naming convention
'''
os = input("What OS is this sheet for?")
df1 = pd.read_csv(filename)
'''
Splits after the period off so universal for mac, linux, and windows
'''
new = df1["Hostname"].str.split(".", n=1, expand=True)
df1["Hostname"] = new[0]
'''
Just sorting
'''
sorted_df = df1.sort_values(by=['Last Seen'])
'''
Shrinking to only Hostname and Serial Number Column
'''
df = sorted_df[["Hostname", "Serial Number"]]
dateObj = datetime.now()
'''
Naming Convention
'''
outputName = "CrowdStrike_" + os + "_" + dateObj.strftime("%m-%d-%Y") + ".csv"
df.to_csv(outputName, index=False)
