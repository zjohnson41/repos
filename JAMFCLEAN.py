import pandas as pd
from datetime import datetime
'''
Asking for filename - rename to simpler if easier - give path if not in same folder
'''
filename = input("What is the filename?")
'''
Naming convention
'''
choice = input("Enter 1 for FileVault, 2 for Duo, or 3 for Active Hosts")
df = pd.read_csv(filename)
'''
Grab SN
'''
df = df[["Serial Number"]]
dateObj = datetime.now()
outputName = ""
'''
Naming
'''
if choice == "1":
    outputName = "Jamf_FileVault_" + dateObj.strftime("%m-%d-%Y") + ".csv"
elif choice == "2":
    outputName = "Jamf_Duo_" + dateObj.strftime("%m-%d-%Y") + ".csv"
elif choice == "3":
    outputName = "Jamf_Active_Hosts_" + dateObj.strftime("%m-%d-%Y") + ".csv"
else:
    pass
df.to_csv(outputName, index=False)