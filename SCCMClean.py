import pandas as pd
from datetime import datetime
'''
Asking for filename - rename to simpler if easier - give path if not in same folder
'''
filename = input("What is the filename?")
'''
For naming convention
'''
choice = input("Enter 1 for Bitlocker, 2 for Duo, or 3 for Client")
df = pd.read_csv(filename, skiprows=3)
'''
Renaming Column
'''
df = df.rename(columns={'Details_Table0_ComputerName': 'Hostname', 'Details_Table0_SerialNumber': 'SerialNumber'})
'''
Grab only needed columns
'''
df = df[["Hostname", "SerialNumber"]]
dateObj = datetime.now()
outputName = ""
if choice == "1":
    outputName = "SCCM_Bitlocker_Enabled_" + dateObj.strftime("%m-%d-%Y") + ".csv"
elif choice == "2":
    outputName = "SCCM_Duo_Installed_" + dateObj.strftime("%m-%d-%Y") + ".csv"
elif choice == "3":
    outputName = "SCCM_Active_Hosts_" + dateObj.strftime("%m-%d-%Y") + ".csv"
else:
    pass
df.to_csv(outputName, index=False)