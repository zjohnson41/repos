import pandas as pd
from datetime import datetime
'''
What is filename? Needs to be in folder or be a path
'''
filename = input("What is the filename?")
os = input("What OS is this sheet for?(Mac, Windows, Linux)")
df = pd.read_csv(filename)
'''
Change deviceName to hostname, in the future deviceName might be changed to deviceOsHostname but theres a lot of 
unknowns in that column now.
'''
df = df.rename(columns={'deviceName': 'Hostname'})
sortedDf = df.sort_values(by=['lastConnectedDate'])
'''
Filter based on status being active and Primary GTRI site
'''
newdf = sortedDf.loc[(sortedDf['status'] == 'Active') & (sortedDf['userStatus'] == 'Active') & (sortedDf['destinationName'] == 'GTRI Primary Site')]
finaldf = newdf[['Hostname']]
'''
Naming convention and end if not mac
'''
dateObj = datetime.now()
outputName = "CrashPlan_" + os + "_" + dateObj.strftime("%m-%d-%Y") + ".csv"
if os.lower() != "mac":
    finaldf.to_csv(outputName, index=False)
    quit()
'''
Basically, this creates a new empty data frame that can add serials to it if they match.
I created an empty list that will add every host name to it, and then iterate through the crosschecking csv file
and check if the host name is in the previously created list, we will add its serial number to the empty data frame, 
build a data frame of all the matching names' serial numbers. Then convert this to a csv and name.
'''
d = {'SerialNumber': []}
serials = pd.DataFrame(data=d)
cross = input("Enter the filename of the file you are using to cross reference. (SN and Hostname)")
check = pd.read_csv(cross)
nameList = []
if os.lower() == "mac":
    for row in finaldf.itertuples():
        nameList.append(row[1])
    for row in check.itertuples():
        if row[1] in nameList:
            serials.loc[len(serials.index)] = [row[2]]
    serials.to_csv(outputName, index=False)