For Data Cleaning files:
CrashPlan clean requires a second CSV file in order to function correctly with Macs. 
In order to work, this file must contain hostnames and serial numbers for Macs.
Windows and Linux work just fine normally.
When using the programs to clean files, make sure the .csv is in the same folder for easiest use.
Enter the filename with the .csv extension on the end, and these programs should clean the report for you into the desired output.
I would suggest changing the report filename to something simple,
so it is easier to type in. The new clean data will be appropriately named regardless and create a new CSV file.
-ZJ